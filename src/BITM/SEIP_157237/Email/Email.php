<?php

namespace App\Email;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Email extends DB
{
    private $id;
    private $user_Name;
    private $email_Id;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("user", $allPostData)) {
            $this->user_Name = $allPostData['user'];
        }

        if (array_key_exists("email", $allPostData)) {
            $this->email_Id = $allPostData['email'];
        }
    }

    public function store(){
        $arrayData = array($this->user_Name, $this->email_Id);
        $query='INSERT INTO `email` (`user`, `email`) VALUES (?,?);';


        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }

    public function index(){
        $sql = "select * from email where del_info='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from email where del_info='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from email where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function edit(){

    }

    public function update(){
        $arrayData = array($this->user_Name, $this->email_Id);
        $query='UPDATE `email` SET `user`=?, `mail`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `email` SET del_info=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `email` SET del_info=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from email where id='.$this->id";
        $this->DBH->exec($sql);
    }


}
