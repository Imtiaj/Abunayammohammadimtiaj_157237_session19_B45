<?php

namespace App\Gender;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Gender extends DB
{
    private $id;
    private $applicant_Name;
    private $gender;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("user", $allPostData)) {
            $this->applicant_Name = $allPostData['user'];
        }

        if (array_key_exists("gender", $allPostData)) {
            $this->gender = $allPostData['gender'];
        }
    }

    public function store(){
        $arrayData = array($this->applicant_Name, $this->gender);
        $query='INSERT INTO `gender` (`user`, `gender`) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }

    public function index(){
        $sql = "select * from gender where del_user='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from gender where del_user='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from gender where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function edit(){

    }

    public function update(){
        $arrayData = array($this->applicant_Name, $this->gender);
        $query='UPDATE `gender` SET `user`=?, `gender`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `gender` SET del_user=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `gender` SET del_user=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from gender where id='.$this->id";
        $this->DBH->exec($sql);
    }


}
