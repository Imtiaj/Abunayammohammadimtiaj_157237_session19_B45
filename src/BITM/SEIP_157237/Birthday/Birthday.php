<?php

namespace App\Birthday;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB
{
    private $id;
    private $Birthday;
    private $date;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("Birthday", $allPostData)) {
            $this->Birthday = $allPostData['Birthday'];
        }

        if (array_key_exists("date", $allPostData)) {
            $this->date = $allPostData['date'];
        }
    }

    public function store(){
        $arrayData = array($this->Birthday, $this->date);
        $query='INSERT INTO `birth_day` (`name`, `birthdate`) VALUES (?,?)';
        
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }
    public function index(){
        $sql = "select * from birth_day where soft_delete='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from birth_day where soft_delete='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from birth_day where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function edit(){

    }

    public function update(){
        $arrayData = array($this->Birthday, $this->date);
        $query='UPDATE `birth_day` SET `name`=?, `birthdate`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `birth_day` SET soft_delete=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `birth_day` SET soft_delete=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from birth_day where id='.$this->id";
        $this->DBH->exec($sql);
    }
}