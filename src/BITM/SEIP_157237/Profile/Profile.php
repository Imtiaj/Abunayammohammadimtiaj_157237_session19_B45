<?php


namespace App\Profile;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Profile extends DB
{
    private $id;
    private $name;
    private $profile_Pic;

    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("User",$allPostData)){
            $this->name= $allPostData['User'];
        }

        if(array_key_exists("picture_name",$allPostData)){
            $this->profile_Pic= $allPostData['picture_name'];
        }
    }
    public function store(){
        $arrData  =  array($this->name,$this->profile_Pic);

        $query= 'INSERT INTO profile ( `user`, `picture`) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been stored successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }

        Utility::redirect('index.php');

    }
    public function view(){

        $sql = "Select * from profile where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from profile where del_pic='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function trashed()
    {
        $sql = "select * from profile where del_pic='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function edit(){

    }

    public function update(){
        $arrayData = array($this->name, $this->profile_Pic);
        $query='UPDATE `profile` SET `user`=?, `picture`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `profile` SET del_pic=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `profile` SET del_pic=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from profile where id='.$this->id";
        $this->DBH->exec($sql);
    }


}