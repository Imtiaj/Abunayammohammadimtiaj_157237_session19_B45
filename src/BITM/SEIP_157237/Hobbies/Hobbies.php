<?php

namespace App\Hobbies;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Hobbies extends DB
{
    private $id;
    private $student_Name;
    private $hob;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("user", $allPostData)) {
            $this->student_Name = $allPostData['user'];
        }

        if (array_key_exists("hobby", $allPostData)) {
            $this->hob = $allPostData['hobby'];
        }
    }

    public function store(){
        $arrayData = array($this->student_Name, $this->hob);
        $query='INSERT INTO `hobby` (`user`, `hobby`) VALUES (?,?)';
        
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }

    public function index(){
        $sql = "select * from hobby where del_info='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from hobby where del_info='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from hobby where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function edit(){

    }

    public function update(){
        $arrayData = array($this->student_Name, $this->hob);
        $query='UPDATE `hobby` SET `user`=?, `hobby`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `hobby` SET del_info=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `hobby` SET del_info=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from hobby where id='.$this->id";
        $this->DBH->exec($sql);
    }


}
