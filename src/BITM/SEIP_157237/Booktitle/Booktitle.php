<?php

namespace App\Booktitle;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Booktitle extends DB
{
    private $id;
    private $Book_title;
    private $author_Name;


    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("Booktitle", $allPostData)) {
            $this->Book_title = $allPostData['Booktitle'];
        }

        if (array_key_exists("authorName", $allPostData)) {
            $this->author_Name = $allPostData['authorName'];
        }

    }

    public function store(){
        $arrayData = array($this->Book_title, $this->author_Name);
        $query='INSERT INTO `book_title` ( `book_name`, `author_name`) VALUES (?,?)';

           $STH = $this->DBH->prepare($query);
           $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }

    public function index(){
        $sql = "select * from book_title where soft_delete='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from book_title where soft_delete='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from book_title where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function edit(){

    }

    public function update(){
        $arrayData = array($this->Book_title, $this->author_Name);
        $query='UPDATE `book_title` SET `book_name`=?, `author_name`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `book_title` SET soft_delete=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `book_title` SET soft_delete=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE FROM `book_title` WHERE `book_title`.`id` ='.$this->id";

        $this->DBH->exec($sql);
    }


}
