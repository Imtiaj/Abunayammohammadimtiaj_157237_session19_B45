<?php

namespace App\Organization;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Organization extends DB
{
    private $id;
    private $org_Name;
    private $sum;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("orgName", $allPostData)) {
            $this->org_Name = $allPostData['orgName'];
        }

        if (array_key_exists("summ", $allPostData)) {
            $this->sum = $allPostData['summ'];
        }
    }

    public function store(){
        $arrayData = array($this->org_Name, $this->sum);
        $query='INSERT INTO `organization` (`org_name`, `summary`) VALUES (?,?)';;
        
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('index.php');
    }

    public function index(){
        $sql = "select * from organization where del_sum='No' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "select * from organization where del_sum='Yes' ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){
        $sql = "select * from organization where id=".$this->id;
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(\PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function edit(){

    }

    public function update(){
        $arrayData = array($this->org_Name, $this->sum);
        $query='UPDATE `organization` SET `org_name`=?, `summary`=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully updated.");
        }
        else{
            Message::message("Update failure");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $arrayData = array("Yes");
        $query='UPDATE `organization` SET del_sum=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully trashed.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('trashed.php');
    }

    public function recover(){
        $arrayData = array("No");
        $query='UPDATE `organization` SET del_sum=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully recovered.");
        }
        else{
            Message::message("Failure");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql= "DELETE from organization where id='.$this->id";
        $this->DBH->exec($sql);
    }


}
