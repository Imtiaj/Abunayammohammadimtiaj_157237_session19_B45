<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../atom.php">Atom</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="../Booktitle/create.php">Book</a></li>
                <li><a href="../Birthday/create.php">Birthday</a></li>
                <li><a href="../City/create.php">City</a></li>
                <li><a href="../Email/create.php">Email</a></li>
                <li><a href="../Gender/create.php">Gender</a></li>
                <li><a href="../Hobbies/create.php">Hobbies</a></li>
                <li class="active"><a href="../Organization/create.php">Organization</a></li>
                <li><a href="../Profilepicture/create.php">Profile</a></li>
            </ul>
        </div>
    </nav>
    <div id="header"><div id="logo"><b><center><h1><u>Organization Summary</u></h1></center></b></div></div>
<form action="store.php" method="post" >

    <h3>Enter Organization Name:</h3>
    <br>
    <input type="text" name="orgName">
    <br>
    <h3>Summary:</h3>
    <textarea name="summ"></textarea>
    <br>
    <input type="submit" value="Submit">

</form>
</div>
</body>

</html>