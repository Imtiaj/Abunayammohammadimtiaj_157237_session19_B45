<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";

$objProfile= new\App\Profile\Profile();
$objProfile->setData($_GET);
$oneData= $objProfile->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div id="main-content" align="center">
    
    <div id="header"><div id="logo"><b><center><h1><u>Profile Information Update</u></h1></center></b></div></div>
    <form name="FileUpload" action="update.php" method="post" enctype="multipart/form-data">

        <h3>Enter Name:</h3>
        <input type="text" name="User" >
        <br>
        <h3>Choose photo to upload:</h3>
        <input type="FILE" name="Picture" placeholder="Choose a File">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input type="submit" value="Update" >
        <a href="index.php" class="btn btn-info">Cancel</a>

    </form>
</div>
</body>


</html>