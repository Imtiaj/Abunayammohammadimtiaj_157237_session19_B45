<?php

require_once("../../../vendor/autoload.php");

use \App\Profile\Profile;

$objProfile = new Profile();
if(isset($_FILES['Picture']['name']))
{
    $picName=time().$_FILES['Picture']['name'];
    $tmp_name=$_FILES['Picture']['tmp_name'];

    move_uploaded_file($tmp_name,'../../Resourses/img/'.$picName);
    $_POST['picture_name']=$picName;
}
$objectPicture= new Profile();
$objectPicture->setData($_POST);
$objectPicture->store();