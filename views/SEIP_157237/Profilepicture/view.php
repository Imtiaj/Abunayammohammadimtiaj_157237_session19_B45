<?php

require_once("../../../vendor/autoload.php");
$objProfile= new\App\Profile\Profile();
$objProfile->setData($_GET);
$oneData= $objProfile->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" href="../../style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1><u>Profile</u></h1></center></b><div align="right"> <a href='index.php' class='btn btn-primary'>Return</a></div></div></div>

    <table class="table table-stripped">
<?php
echo "


        <tr>
            <td>Id:</td>
            <td>$oneData->id</td>
        </tr>
        <tr>
            <td>Name:</td>
            <td>$oneData->user</td>
        </tr>
        <tr>
            <td>Picture:</td>
            <td><img src='../../Resourses/img/$oneData->picture' width='50px'></td>
        </tr>



";

?>
</table>
    </div>
</body>
</html>
