<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";

$objBirthday= new\App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData= $objBirthday->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday(Update)</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1><u>Birthday (Update)</u></h1></center></b></div></div>
    <form action="update.php" method="post" >

        <h3>Enter Name:</h3>
        <input type="text" name="Birthday" value="<?php echo $oneData->name ?>">
        <br>
        <h3>Enter Birth Date:</h3>
        <input type="text" name="date" value="<?php echo $oneData->birthdate ?>">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input type="submit" value="update">
        <input type="submit" value="cancel"<a href="index.php"></a> 

    </form>
</div>
<script src="../../../resource/bootstrap/js/­bootstrap.min.js"></script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>
</body>

</html>