<?php

require_once("../../../vendor/autoload.php");
$objBirthday= new\App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData= $objBirthday->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day List</title>
    <link rel="stylesheet" href="../../style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1><u>Birthday</u></h1></center></b><div align="right"> <a href='index.php' class='btn btn-primary'>Return</a></div></div></div>
<table class="table table-stripped">
<?php
echo "


        <tr>
            <td>Id:</td>
            <td>$oneData->id</td>
        </tr>
        <tr>
            <td>Name:</td>
            <td>$oneData->name</td>
        </tr>
        <tr>
            <td>Birth Date:</td>
            <td>$oneData->birthdate</td>
            
        </tr>



";

?>
</table>
    </div>
</body>
</html>
