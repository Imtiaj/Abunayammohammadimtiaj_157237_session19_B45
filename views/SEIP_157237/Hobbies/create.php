<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../atom.php">Atom</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="../Booktitle/create.php">Book</a></li>
                <li><a href="../Birthday/create.php">Birthday</a></li>
                <li><a href="../City/create.php">City</a></li>
                <li><a href="../Email/create.php">Email</a></li>
                <li><a href="../Gender/create.php">Gender</a></li>
                <li class="active"><a href="../Hobbies/create.php">Hobbies</a></li>
                <li><a href="../Organization/create.php">Organization</a></li>
                <li><a href="../Profilepicture/create.php">Profile</a></li>
            </ul>
        </div>
    </nav>
    <div id="header"><div id="logo"><b><center><h1><u>Hobbies</u></h1></center></b></div></div>
<form action="store.php" method="post" >

    <h3>Student Name:</h3>
    <input type="text" name="user">
    <br>
    <h3>Hobbies:</h3>
    <input type="checkbox" name="hobby" value="Painting">Painting
    <input type="checkbox" name="hobby" value="Cycling">Cycling
    <input type="checkbox" name="hobby" value="Swimming">Swimming
    <input type="checkbox" name="hobby" value="Travelling">Travelling


    <br>
    <input type="submit" value="Submit">

</form>

</body>
<script src="../../../resource/bootstrap/js/­
jquery-3.1.1.min.js"></script>
<script src="../../../resource/bootstrap/js/­
bootstrap.min.js"></script>
<script>
    jQuery(function($) {
        $('#msg').fadeOut (550);
        $('#msg').fadeIn (550);
        $('#msg').fadeOut (550);
        $('#msg').fadeIn (550);
        $('#msg').fadeOut (550);
        $('#msg').fadeIn (550);
        $('#msg').fadeOut (550);
    })
</script>
</html>