<?php
require_once("../../../vendor/autoload.php");
$objGender= new\App\Gender\Gender();
$allData= $objGender->trashed();
use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1>Gender Hidden List</h1></center></b><div id="right" align="right"><td><a href=index.php class='btn btn-info'>Back</a></td></div></div></div>

    <table class="table table-stripped ">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Applicant Name</th>
        <th>Gender</th>
        <th>Action Buttons</th>
    </tr>
    <?php
    $serial=1;
    foreach($allData as $oneData){
        echo"
                                <tr>
                                    <td>$serial</td>
                                    <td>$oneData->id</td>
                                    <td>$oneData->user</td>
                                    <td>$oneData->gender</td>

                                    <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                    <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a></td>
                                 </tr>
                        ";
        $serial++;
    }

    ?>

</table>
</div>
</body>
</html>