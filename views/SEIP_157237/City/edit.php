<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";

$objCity= new\App\City\City();
$objCity->setData($_GET);
$oneData= $objCity->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Entries</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    
    <div id="header"><div id="logo"><b><center><h1><u>City Update</u></h1></center></b></div></div>

    <form action="update.php" method="post" >

        <h4>Choose City:</h4>
        <select name="city">
            <option value="chittagong">Chittagong</option>
            <option value="dhaka">Dhaka</option>
            <option value="sylhet">Sylhet</option>
            <option value="rajshahi">Rajshahi</option>
            <option value="Comilla">Comilla</option>
            <option value="khulna">Khulna</option>
            <option value="barisal">Barisal</option>
        </select>
        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input type="submit" class="btn btn-info" value="Update">
        <a href="index.php" class="btn btn-info">Cancel</a>
    

    </form>
</div>
</body>
</html>