<?php
require_once("../../../vendor/autoload.php");
$objCity= new\App\City\City();
$allData= $objCity->trashed();
use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Deleted City List</title>
    <link rel="stylesheet" href="../../style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1>Trashed City</h1></center></b><div id="right" align="right"><td><a href=index.php class='btn btn-info'>Back</a></td></div></div></div>

    <table class="table table-stripped ">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>City</th>
        <th>Action Buttons</th>
    </tr>
    <?php
    $serial=1;
    foreach($allData as $oneData){
        echo"
                                <tr>
                                    <td>$serial</td>
                                    <td>$oneData->id</td>
                                    <td>$oneData->City</td>

                                    <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                   <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a></td>
                                 </tr>
                        ";
        $serial++;
    }

    ?>

</table>
</div>
</body>
</html>