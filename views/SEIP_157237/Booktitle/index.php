<?php
require_once("../../../vendor/autoload.php");
$objBooktitle= new\App\Booktitle\Booktitle();
$allData= $objBooktitle->index();
use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../atom.php">Atom</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="../Booktitle/create.php">Book</a></li>
                <li><a href="../Birthday/create.php">Birthday</a></li>
                <li><a href="../City/create.php">City</a></li>
                <li><a href="../Email/create.php">Email</a></li>
                <li><a href="../Gender/create.php">Gender</a></li>
                <li><a href="../Hobbies/create.php">Hobbies</a></li>
                <li><a href="../Organization/create.php">Organization</a></li>
                <li><a href="../Profilepicture/create.php">Profile</a></li>
            </ul>
        </div>
    </nav>
    <div id="header"><div id="logo"><b><center><h1>Book List</h1></center></b><div id="right" align="right"><a href=trashed.php class='btn btn-info'>Trash</a></div></div></div>



<table class="table table-stripped ">
            <tr>
            <th>Serial Number</th>
            <th>ID</th>
            <th>Book Name</th>
            <th>Author Name</th>
                <th>Action Buttons</th>
            </tr>
                <?php
                $serial=1;
                    foreach($allData as $oneData){
                        echo"
                                <tr>
                                    <td>$serial</td>
                                    <td>$oneData->id</td>
                                    <td>$oneData->book_name</td>
                                    <td>$oneData->author_name</td>

                                    <td>
                                    <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                    <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                    <a href='trash.php?id=$oneData->id' class='btn btn-warning'>S_Del</a>
                                    <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Del</a>
                                    </td>
                                 </tr>
                        ";
                        $serial++;
                    }

            ?>

        </table>
    </div>
</body>

</html>