<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";

$objBooktitle= new\App\Booktitle\Booktitle();
$objBooktitle->setData($_GET);
$oneData= $objBooktitle->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title(Update)</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1><u>Book Title (Update)</u></h1></center></b></div></div>
    <form action="update.php" method="post" >

        <h3>Enter Bookname:</h3>
        <input type="text" name="Booktitle" value="<?php echo $oneData->book_name ?>">
        <br>
        <h3>Enter Author name:</h3>
        <input type="text" name="authorName" value="<?php echo $oneData->author_name ?>">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input type="submit" value="update">

    </form>
</div>
<script src="../../../resource/bootstrap/js/­
jquery-3.1.1.min.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>
</body>

</html>