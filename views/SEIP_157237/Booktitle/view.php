<?php

require_once("../../../vendor/autoload.php");
$objBooktitle= new\App\Booktitle\Booktitle();
$objBooktitle->setData($_GET);
$oneData= $objBooktitle->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
    <div id="header"><div id="logo"><b><center><h1><u>Book's</u></h1></center></b><div align="right"> <a href='index.php' class='btn btn-primary'>Return</a></div></div></div>

<table class="table table-stripped">
<?php
echo "


        <tr>
            <td>Id:</td>
            <td>$oneData->id</td>
        </tr>
        <tr>
            <td>Book Name:</td>
            <td>$oneData->book_name</td>
        </tr>
        <tr>
            <td>Author Name:</td>
            <td>$oneData->author_name</td>
        </tr>



";

?>
</table>
    </div>
</body>
</html>
