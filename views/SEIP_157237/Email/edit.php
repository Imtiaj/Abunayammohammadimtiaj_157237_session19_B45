<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();
echo "<div id='message'>$msg</div>";

$objEmail= new\App\Email\Email();
$objEmail->setData($_GET);
$oneData= $objEmail->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Info</title>
    <link rel="stylesheet" type="text/css" href="../../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div id="main-content" align="center">
 
    <div id="header"><div id="logo"><b><center><h1><u>Email Update</u></h1></center></b></div></div>
    <form action="update.php" method="post" >

        <h3>Enter Your Name:</h3>
        <input type="text" name="user">
        <br>
        <h3>Enter Your Email:</h3>
        <input type="text" name="email">
        <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input type="submit" value="Update">
        <a href="index.php" class="btn btn-info">Cancel</a>

    </form>
</div>
</body>
<script src="../../../resource/bootstrap/js/­
jquery-3.1.1.min.js"></script>
<script src="../../../resource/bootstrap/js/­
bootstrap.min.js"></script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>
</html>